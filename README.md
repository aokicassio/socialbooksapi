# ABOUT API #

This is a simple demonstration of a Restful API using Spring Framework. 

The API create books, authors and add comments to books. The system also delete and edit books.

**List Books / Listar Livros**

GET to: http://localhost:8080/livros/

(no body)

**List Authors / Listar Autores**

GET to: http://localhost:8080/autores/

(no body)

**Register an Author / Registrar um Autor**

POST to: http://localhost:8080/autores/

```
#!json
{
    "nome" : "Cássio Luiz Aoki",
    "nascimento" : "22/01/1990",
    "nacionalidade" : "Brasil"
}
```

**Register a Book / Registrar um Livro**

POST to : http://localhost:8080/livros/

```
#!json

{
    "nome" : "Spring RESTful API",
    "autor" : {"id" : "1"},
    "editora" : "Pivotal",
    "publicacao" : "22/01/1990",
    "resumo" : "Book about Spring API Restful"
}
```

**Add comment to a book / Adicionar comentário a um Livro**

POST to: http://localhost:8080/livros/{id}/comentarios


```
#!json

{
    "comentario" : "I recommend this book to all the java developers community!"
}

```


**Book Details / Detalhes de Livro**

GET to: http://localhost:8080/livros/{id} 

(no body)

**See Book comments / Ver comentários de Livro**

GET to: http://localhost:8080/livros/{id}/comentarios 

(no body)

**Delete a Book / Deletar um Livro**

DELETE to: http://localhost:8080/livros/{id}/ 

(no body)

**Edit a Book / Editar um Livro**

PUT to: http://localhost:8080/livros/{id}/

```
#!json
{
    "nome" : "Spring RESTful API",
    "autor" : {"id" : "1"},
    "editora" : "Pivotal",
    "publicacao" : "22/01/1990",
    "resumo" : "New book description"
}
```